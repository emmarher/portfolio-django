from django.shortcuts import render
from .models import Project

# Create your views here.
def home(request):

    #traer de la base de datos
    projects = Project.objects.all()

    return render(request, 'home.html', {'projects':projects})
