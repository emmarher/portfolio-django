from django import views
from django.urls import path
from .views import render_post, post_detail
# import views

# Para referenciar todas las url utilizamos el nombre de una variable, post visita <url patterns>

app_name= 'blog'

urlpatterns = [
    path('', render_post, name='posts'),
    path('<int:post_id>', post_detail, name='post_detail'),
    
]