
from django.db import models
import datetime

# Create your models here.
class Post(models.Model):
    #Mostrar el titulo en admin site - NO como objeto

    def __str__(self):
        return self.title

    title = models.CharField(max_length=100)
    description = models.TextField()#contenido mas grande
    image = models.ImageField(upload_to='blog/images')
    date = models.DateField(datetime.date.today)
