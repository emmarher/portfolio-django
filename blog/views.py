from django.shortcuts import render, get_object_or_404
from .models import Post
#intenta obtener un objeto si no muestra 404 get_objet_or_404
# Create your views here.
def render_post(request):

    posts = Post.objects.all()
    return render(request,'post.html', {'posts': posts})

def post_detail(request, post_id):

    post = get_object_or_404(Post, pk=post_id)

    return render(request, 'post_detail.html', {'post':post})